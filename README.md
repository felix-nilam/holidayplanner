# Holiday Planner App

Holiday Planner is a Salesforce App to set a budget for holiday/yearly shopping and track expenses against the budget.  

## Overview

This app was implemented as a Salesforce learning project to create an app, mostly declaratively, to demonstrate the out-of-the-box capabilities which include Custom Object, Rollup-Summary, Salesforce Flow, Dynamic Forms, Reports & Dashboards, Salesforce mobile app, and more.  This is intended as a playground to test various Salesforce features.

## Application Design

![Data Model](docs/images/HolidayPlannerDataModel.jpg)

The design is based on the following key components:

* Holiday is the main object to capture the planning (multiple Budgets) and tracking (multiple Expenses)
* Budget and Expense setup as Master-Detail to utilise Rollup-Summary in Holiday to get the budget total, actual total and budget variance
* 3 personas created to track budget summary separately (Felix, Nina and Bruce)
* Dynamic Forms used in Holiday Record Page to have the budget summary for overall and the 3 personas in separate tab
* Before Save Flow created in Expense to store the related Contact's First Name, which is used in Holiday Rollup-Summary custom fields for the 3 personas to get the total expenses for the individuals
* Standard Reports used to be added in Holiday Record Page and Dashboard
* Custom Ligntning Actions used in Holiday as shortcut to add Shopping/Transport/Food/Misc in mobile app to easily add Expense against a particular Holiday

## Installation

You can install the Holiday Planner app to your Salesforce Org by using SFDX.

Pre-requisites:

* Login to your Salesforce Org using sfdx force:auth
* Enable Multi Currency in your target Salesforce Org
* git clone this repo

Run the following command to deploy the app:
```
sfdx force:source:deploy -p force-app/main/default
```

## Usage

To use the Holiday Planner app, setup the following:

* Assign the "Holiday Admin" Permission Set to the user that can setup the Holiday and Budgets
* Assign the "Holiday User" Permission Set to the user that will be tracking their expenses against the Holiday
* Create the Contacts for which you want to track the Expenses
* Modify the Holiday Rollup-Summary custom fields (Bruce Actual Total, Bruce Budget Total, Felix Actual Total, Felix Budget Total, Nina Actual Total, Nina Budget Total) to have the correct Name of your Contacts.  You can add more custom fields if you need to track more than 3 people
* Once Holiday is created and Budgets are created for the Holiday, set the Holiday Status to "In Progress"
* Using the Salesforce mobile app, users can now add Expenses by going to the Holiday record and using the "Add Shopping" button

## License

[MIT](https://choosealicense.com/licenses/mit/)

## Want to learn more?

Head over to [Trailhead](https://trailhead.salesforce.com/) to learn more about building apps on Salesforce.
Happy Learning!